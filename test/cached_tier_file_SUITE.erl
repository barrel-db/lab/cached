%%%-------------------------------------------------------------------
%%% @author benoitc
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 31. Oct 2017 17:03
%%%-------------------------------------------------------------------
-module(cached_tier_file_SUITE).
-author("benoitc").

-define(ftest, "cached_test.log").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/1
]).

-export([
  basic/1,
  iterator/1
]).


all() ->
  [
    basic,
    iterator
  ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(cached),
  _ = cached_tier_file:delete(?ftest),
  Config.

end_per_suite(Config) ->
  ok = application:stop(cached),
  Config.


init_per_testcase(_, Config) ->
  ok = cached_tier_file:delete(?ftest),
  Config.

end_per_testcase(_Config) ->
  ok.

basic(_Config) ->
  {ok, Writer} = cached_tier_file:open(?ftest),
  {ok, Reader} = cached_tier_file:open_read(?ftest),
  {ok, Pos1, Size1} = cached_tier_file:append(Writer, a, b),
  {ok, a, b} = cached_tier_file:read(Reader, Pos1, Size1),
  {ok, Pos2, Size2} = cached_tier_file:append(Writer, c, d),
  {ok, c, d} = cached_tier_file:read(Reader, Pos2, Size2),
  ok = cached_tier_file:close(Writer),
  ok = cached_tier_file:close(Reader),
  {ok, Reader2} = cached_tier_file:open(?ftest),
  {ok, a, b} = cached_tier_file:read(Reader2, Pos1, Size1),
  {ok, c, d} = cached_tier_file:read(Reader2, Pos2, Size2),
  ok = cached_tier_file:close(Reader2),
  ok.

iterator(_Config) ->
  {ok, Fd} = cached_tier_file:open(?ftest),
  {ok, _Pos1, _Size1} = cached_tier_file:append(Fd, a, b),
  {ok, _Pos2, _Size2} = cached_tier_file:append(Fd, c, d),
  Itr = cached_tier_file:iterator(Fd),
  {ok, {a, b}, Itr1} = cached_tier_file:next(Itr),
  io:format("itr # is ~p~n", [Itr1]),
  {ok, {c, d}, Itr2} = cached_tier_file:next(Itr1),
  eof =  cached_tier_file:next(Itr2),
  ok = cached_tier_file:close(Fd),
  ok.
  
  

  
  
  
  
  
  
  
  
  
  
  
  
  
