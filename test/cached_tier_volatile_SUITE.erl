%%%-------------------------------------------------------------------
%%% @author benoitc
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Oct 2017 14:43
%%%-------------------------------------------------------------------
-module(cached_tier_volatile_SUITE).
-author("benoitc").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/1
]).

-export([
  basic_usage/1,
  evict_fun/1
]).


all() ->
  [
    basic_usage,
    evict_fun
  ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(cached),
  Config.

end_per_suite(Config) ->
  ok = application:stop(cached),
  Config.


init_per_testcase(_, Config) ->
  Config.

end_per_testcase(_Config) ->
  ok.


basic_usage(_) ->
  Options =  [{max_memory, 18}],
  {ok, _Pid} = cached_tier_volatile:start_link(lru_cache_test,Options),
  true = cached_tier_volatile:insert(lru_cache_test, a, b),
  {ok, b} = cached_tier_volatile:lookup(lru_cache_test, a),
  1 = cached_tier_volatile:get_num_entries(lru_cache_test),
  true = cached_tier_volatile:insert(lru_cache_test, b, c),
  {ok, c} = cached_tier_volatile:lookup(lru_cache_test, b),
  2 = cached_tier_volatile:get_num_entries(lru_cache_test),
  true = cached_tier_volatile:insert(lru_cache_test, d, e),
  timer:sleep(100),
  2 = cached_tier_volatile:get_num_entries(lru_cache_test).

evict_fun(_) ->
  _ = ets:new(lru_test, [named_table, ordered_set, public]),
  Options =  [
    {max_memory, 18},
    {evict_fun,
     fun(Key, Value) ->
       ets:insert(lru_test, {Key, Value})
     end
    }
  ],
  {ok, _Pid} = cached_tier_volatile:start_link(lru_cache_test, Options),
  true = cached_tier_volatile:insert(lru_cache_test, a, b),
  true = cached_tier_volatile:insert(lru_cache_test, b, c),
  true = cached_tier_volatile:insert(lru_cache_test, d, e),
  timer:sleep(100),
  [{a, b}] = ets:lookup(lru_test, a),
  ok.