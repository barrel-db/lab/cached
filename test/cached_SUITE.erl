-module(cached_SUITE).
-author("benoitc").
%% API
%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).
-export([
  t_basic/1,
  t_balancing/1,
  t_client/1
]).

all() ->
  [
    t_basic,
    t_balancing,
    t_client
  ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(cached),
  {ok, RemoteNode} = start_slave(test_cached2),
  [{remote, RemoteNode} | Config].
end_per_suite(Config) ->
  ok = stop_slave(test_cached2),
  Config.

init_per_testcase(_, Config) ->
  {ok, Peers} = start_local_cache(),
  ok = start_remote_cache(remote(Config)),
  [{peers, Peers} | Config].
end_per_testcase(_, Config) ->
  ok = stop_local_cache(),
  ok = stop_remote_cache(remote(Config)),
  ok.


%% ==============================
%% tests

%% @doc test the cache distribution
t_basic(Config) ->
  true = cached:insert(test, a, b),
  {ok, b} = cached:lookup(test, a),
  {ok, b} = rpc:call(remote(Config), cached, lookup, [test, a]),
  true = rpc:call(remote(Config), cached, insert, [test, b, d]),
  {ok, d} = cached:lookup(test, b),
  true = cached:delete(test, a),
  error = rpc:call(remote(Config), cached, lookup, [test, a]),
  ok.

%% @doc test if the keys are balanced
t_balancing(Config) ->
  true = cached:insert(test, a, b),
  true = cached:insert(test, b, d),
  
  1 = cached:local_num_entries(test),
  1 = rpc:call(remote(Config), cached, local_num_entries, [test]),
  ok.

t_client(Config) ->
  true = cached_client:insert(peers(Config), test, a, b),
  {ok, b} = cached_client:lookup(peers(Config), test, a),
  {ok, b} = cached:lookup(test, a),
  {ok, b} = rpc:call(remote(Config), cached, lookup, [test, a]),
  ok.


%% ==============================
%% internal helpers

remote(Config) ->
  Remote = proplists:get_value(remote, Config),
  Remote.

peers([{peers, Peers} | _]) -> Peers.

start_slave(Node) ->
  {ok, HostNode} = ct_slave:start(Node,
                                  [{kill_if_fail, true}, {monitor_master, true},
                                   {init_timeout, 3000}, {startup_timeout, 3000}]),
  pong = net_adm:ping(HostNode),
  CodePath = filter_rebar_path(code:get_path()),
  true = rpc:call(HostNode, code, set_path, [CodePath]),
  {ok,_} = rpc:call(HostNode, application, ensure_all_started, [cached]),
  ct:print("\e[32m ---> Node ~p [OK] \e[0m", [HostNode]),
  {ok, HostNode}.

stop_slave(Node) ->
  {ok, _} = ct_slave:stop(Node),
  ok.

start_local_cache() ->
  {ok, Peers} = cached_pg2:new(),
  ok = cached:new_groupcache(test, Peers, [{max_memory, 18}]),
  {ok, Peers}.

stop_local_cache() ->
  cached:delete_groupcache(test).

start_remote_cache(Node) ->
  {ok, Peers} = rpc:call(Node, cached_pg2, new, []),
  rpc:call(Node, cached, new_groupcache,  [test, Peers, [{max_memory, 18}]]).

stop_remote_cache(Node) ->
  rpc:call(Node, cached, delete_groupcache, [test]).

%% a hack to filter rebar path
%% see https://github.com/erlang/rebar3/issues/1182
filter_rebar_path(CodePath) ->
  lists:filter(
    fun(P) ->
      case string:str(P, "rebar3") of
        0 -> true;
        _ -> false
      end
    end,
    CodePath
  ).

