%%%-------------------------------------------------------------------
%%% @author benoitc
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Oct 2017 14:43
%%%-------------------------------------------------------------------
-module(cached_lru_SUITE).
-author("benoitc").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/1
]).

-export([
  basic_usage/1,
  evict_fun/1
]).


all() ->
  [
    basic_usage,
    evict_fun
  ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(cached),
  Config.

end_per_suite(Config) ->
  ok = application:stop(cached),
  Config.


init_per_testcase(_, Config) ->
  Config.

end_per_testcase(_Config) ->
  ok.

basic_usage(_) ->
  Options =  [{max_memory, 18}, {max_memory_sample, 1}, {gc_interval, 10}],
  {ok, _Pid} = cached_lru:start_link(lru_cache_test,Options),
  true = cached_lru:insert(lru_cache_test, a, b),
  {ok, b} = cached_lru:lookup(lru_cache_test, a),
  9 = cached_lru:get_usage(lru_cache_test),
  true = cached_lru:insert(lru_cache_test, b, c),
  {ok, c} = cached_lru:lookup(lru_cache_test, b),
  18 = cached_lru:get_usage(lru_cache_test),
  true = cached_lru:insert(lru_cache_test, d, e),
  timer:sleep(100),
  18 = cached_lru:get_usage(lru_cache_test).

evict_fun(_) ->
  _ = ets:new(lru_test, [named_table, ordered_set, public]),
  ets:insert_new(lru_test, {evict_count, 0}),
  Options =  [
    {max_memory, 18},
    {max_memory_sample, 1},
    {gc_interval, 10},
    {evict_fun,
       fun(_Key, _Value) ->
         ets:update_counter(lru_test, evict_count, {2, 1})
       end
     }
  ],
  {ok, _Pid} = cached_lru:start_link(lru_cache_test, Options),
  true = cached_lru:insert(lru_cache_test, a, b),
  true = cached_lru:insert(lru_cache_test, b, c),
  true = cached_lru:insert(lru_cache_test, d, e),
  timer:sleep(100),
  1 = ets:update_counter(lru_test, evict_count, {2, 0}),
  ok.
  
  
  
  
  
  