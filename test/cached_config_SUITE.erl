%%
%% Copyright (C) 2017 Benoit Chesneau
%%

-module(cached_config_SUITE).
-author("benoitc").

%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/1
]).

-export([
  basic/1,
  badarg/1
]).


all() ->
  [
    basic,
    badarg
  ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(cached),
  Config.

end_per_suite(Config) ->
  ok = application:stop(cached),
  Config.


init_per_testcase(_, Config) ->
  Config.

end_per_testcase(_Config) ->
  ok.

basic(_Config) ->
  undefined = cached_config:get(a),
  ok = cached_config:put(a, #{ b => c }),
  #{ b := c } = cached_config:get(a),
  ok = cached_config:delete(a),
  undefined = cached_config:get(a),
  ok.

badarg(_Suite) ->
  ok = cached_config:put(a, #{ b => c }),
  try cached_config:put(a, #{ b => self() })
  catch
    error:badarg -> ok
  end,
  #{ b := c } = cached_config:get(a),
  ok.
  