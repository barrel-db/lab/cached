.PHONY: all init upgrade compile clean test xref shell dialyzer tags
PROJECT ?= $(notdir $(CURDIR))
PROJECT := $(strip $(PROJECT))

REBAR = ${PWD}/scripts/rebar3
EPMD ?= $(shell which epmd)

all: test dialyzer

init: $(REBAR)

compile: init
	@${REBAR} compile

clean: init
	@${REBAR} clean --all

test: init
	@$(EPMD) -daemon
	@${REBAR} ct --sname test_cached

shell: init
	@${REBAR} shell

dialyzer: init
	@${REBAR} dialyzer

scripts:
	mkdir $@

$(REBAR): | scripts
	curl -L -o $@ https://s3.amazonaws.com/rebar3/rebar3
	chmod +x $@