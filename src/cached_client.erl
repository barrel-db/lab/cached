%%
%% Copyright (C) 2017 Benoit Chesneau
%%

%% @doc simple client to store/retrieve from a cache
%% without joining the group. The nodes will need to be
%% connected to one of the member and the group created.

-module(cached_client).
-author("benoitc").

%% API
-export([
  insert/4,
  lookup/3,
  delete/3
]).


insert(Peers, Cache, Key, Value) ->
  call(Peers, insert, [Cache, Key, Value]).

lookup(Peers, Cache, Key) ->
  call(Peers, lookup, [Cache, Key]).

delete(Peers, Cache, Key) ->
  call(Peers, delete, [Cache, Key]).


%% --------------
%% private

call(Peers, Fun, Args) ->
  #{ backend := Backend } = Peers,
  apply(Backend, Fun, [Peers] ++ Args).