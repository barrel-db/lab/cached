%%
%% Copyright (C) 2017 Benoit Chesneau
%%
-module(cached_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  Config = #{
    id => config,
    start => {cached_config, start_link, []},
    restart => transient,
    shudown => 2000,
    type => worker,
    modules => [cached_config]
  },
  CacheSup = #{
    id => scache_sup,
    start => {cached_cache_sup, start_link, []},
    restart => permanent,
    shutdown => infinity,
    type => supervisor,
    modules => [cached_cache_sup]
  },
  
  
  SupFlags = #{
    strategy => one_for_all,
    intensity => 4,
    period =>3600
  },
  {ok, {SupFlags, [Config, CacheSup]} }.

%%====================================================================
%% Internal functions
%%====================================================================
