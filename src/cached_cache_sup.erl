%%
%% Copyright (C) 2017 Benoit Chesneau
%%

-module(cached_cache_sup).
-author("benoitc").
-behaviour(supervisor).

%% API
-export([start_link/0]).

%% supervisor callbacks
-export([init/1]).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  {ok, {
    #{ strategy => simple_one_for_one, intensity => 3, period => 3600  },
    [#{
       id => cache,
       start => {cached, start_link, []},
       restart => transient,
       shutdown => 5000,
       type => supervisor,
       modules => [cached]
     }]
  }}.
  