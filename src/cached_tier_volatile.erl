%%
%% Copyright (C) 2017 Benoit Chesneau
%%

%% @doc memory cache using a simpler but more exact implementation
%% than cached_lru since the lru functions are somewhat blocking
%% via the gen_server

%% TODO: shard ETS ?

-module(cached_tier_volatile).
-author("benoitc").
-behaviour(cached_cache).
-behaviour(gen_server).

%% API
-export([
  start_link/2,
  insert/3,
  lookup/2,
  delete/2,
  get_usage/1,
  get_pinned_usage/1,
  get_num_entries/1
]).


%% gen_server callbacks
-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3
]).


-define(MAX_MEMORY, 4*1024*1024).

%% ------------------
%% cache api

insert(Name, Key, Value) ->
  gen_server:call(Name, {insert, Key, Value}).
  
lookup(Name, Key) ->
  gen_server:call(Name, {lookup, Key}).

delete(Name, Key) ->
  gen_server:call(Name, {delete, Key}).

start_link(Name, Options) ->
  gen_server:start_link({local, Name}, ?MODULE, [Name, Options], []).

get_usage(Name) ->
  gen_server:call(Name, get_usage).

get_pinned_usage(Name) ->
  gen_server:call(Name, get_pinned_usage).

get_num_entries(Name) ->
  gen_server:call(Name, num_entries).


%% ------------------
%%  gen_server callbacks


init([Name, Options]) ->
  Tab = ets:new(Name, ets_options(Options)),
  %% get initial usage
  Sz = ets:info(Tab, memory),
  MaxMemory = proplists:get_value(max_memory, Options),
  EvictFun = proplists:get_value(evict_fun, Options),
  {ok, #{tab => Tab,
         name => Name,
         max_memory => MaxMemory,
         lru => cached_lrulist:new(),
         memory_0 => Sz,
         evict_fun => EvictFun
    }}.

handle_call({insert, Key, Val}, From, State =  #{ tab := Tab, lru := Lru }) ->
  true = ets:insert(Tab, {Key, Val}),
  gen_server:reply(From, true),
  Lru2 = cached_lrulist:push(Key, Lru),
  NewState = case has_exceeded(State) of
               true ->
                 evict(State#{ lru => Lru2 });
               false ->
                 State#{ lru => Lru2 }
             end,
  {noreply, NewState};

handle_call({lookup, Key}, From, State = #{ tab := Tab, lru := Lru }) ->
  case ets:lookup(Tab, Key) of
    [{Key, Val}] ->
      gen_server:reply(From, {ok, Val}),
      Lru2 = cached_lrulist:touch(Key, Lru),
      {noreply, State#{ lru => Lru2 }};
    [] ->
      {reply, error, State}
  end;

handle_call({delete, Key}, From, State =  #{ tab := Tab, lru := Lru }) ->
  _ = ets:delete(Tab, Key),
  gen_server:reply(From, ok),
  Lru2 = cached_lrulist:drop(Key, Lru),
  {reply, true, State#{ lru => Lru2 }};

handle_call({evict, Key},  _From, State =  #{ lru := Lru }) ->
  Lru2 = cached_lrulist:touch(Key, Lru),
  {reply, ok, State#{ lru => Lru2 }};

handle_call({set_evict_fun, Fun}, _From, State) ->
  {reply, ok, State#{ evict_fun => Fun}};

handle_call(get_usage, _From, State) ->
  {reply, cache_usage(State), State};

handle_call(get_pinned_usage, _From, State = #{ tab := Tab }) ->
  {reply, ets:info(Tab, memory) , State};

handle_call(num_entries, _From, State = #{ tab := Tab }) ->
  {reply, ets:info(Tab, size) , State};

handle_call(Msg, _From, State) ->
  {reply, {bad_call, Msg}, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(_Info, State) ->
  {noreply, State}.

terminate(_Reason, _State) ->
  ok.

code_change(_Vsn, State, _Extra) ->
  {ok, State}.


%% ------------------
%% init helpers

ets_options(Options) ->
  case proplists:get_value(compressed, Options) of
    true -> [ordered_set, public, compressed];
    _ -> [ordered_set, public]
  end.

%% ------------------
%% cache helpers

cache_usage(#{tab := Tab, memory_0 := Mem0 }) ->
  ets:info(Tab, memory) - Mem0.

has_exceeded(#{ tab := Tab, max_memory := MaxMemory, memory_0 := Mem0 }) ->
  Used = ets:info(Tab, memory) - Mem0,
  (Used > MaxMemory).

evict(State = #{ lru := Lru}) ->
  case cached_lrulist:pop(Lru) of
    {ok, Key, Lru2} ->
      ok = evict_1(Key, State),
      State#{ lru => Lru2 };
    empty ->
      State
  end.

evict_1(Key, #{tab := Tab, evict_fun := undefined}) ->
  _ = ets:delete(Tab, Key),
  ok;
evict_1(Key, #{ tab := Tab, evict_fun := EvictFun}) ->
  case ets:take(Tab, Key) of
    [{Key, Value}] ->
      _ = spawn(fun() -> EvictFun(Key, Value) end),
      ok;
    [] ->
      ok
  end.