%%
%% Copyright (C) 2017 Benoit Chesneau
%%
%% @doc pg2 backend to get the list of the peers. You need create to manually the group
%%


-module(cached_pg2).
-author("benoitc").
-behaviour(cached_peer).

%% API
-export([
  new/0,
  join/3
]).

%% cached_peer behaviour callbacks
-export([
  pick_peer/3,
  lookup/3,
  insert/4,
  delete/3
]).

new() ->
  {ok, #{ backend => ?MODULE }}.

join(_, GroupName, Pid) ->
  ok = pg2:create(GroupName),
  pg2:join(GroupName, Pid).

pick_peer(_, Cache, Key) ->
  Peers = lists:usort(pg2:get_members(Cache)),
  Id = erlang:phash2(Key, length(Peers)),
  element(Id + 1, list_to_tuple(Peers)).

lookup(Peers, Cache, Key) ->
  Peer = pick_peer(Peers, Cache, Key),
  rpc:call(node(Peer), cached, local_lookup, [Cache, Key]).

insert(Peers, Cache, Key, Value) ->
  Peer = pick_peer(Peers, Cache, Key),
  rpc:call(node(Peer), cached, local_insert, [Cache, Key, Value]).

delete(Peers, Cache, Key) ->
  Peer = pick_peer(Peers, Cache, Key),
  rpc:call(node(Peer), cached, local_delete, [Cache, Key]).