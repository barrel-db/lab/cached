%%
%% Copyright (C) 2017 Benoit Chesneau
%%


%% @doc generic behaviour for a local cache backend.

-module(cached_cache).
-author("benoitc").

-type cache_options() :: proplists:proplists().

-export_types([cache_options/0]).

%% start a process keeping the cache state
-callback start_link(Name :: atom(), Options :: cache_options()) -> {ok, Pid :: pid()} | {error, Reason :: any()}.

%% insert in the cache a key. Cache must be found by its name.
-callback insert(Name :: atom(), Key :: term(), Value :: term()) -> true | {error, term()}.

%% get a key from the cache
-callback lookup(Name :: atom(), Key :: term()) -> {ok, Val :: term()} | error.

%% delete/evict a key in the cache
-callback delete(Name :: atom(), Key :: term()) -> true.

%% returns the memory size for the entries residing in the cache.
-callback get_usage(Name :: atom()) -> Usage :: non_neg_integer().

%% returns the memory size for the entries in use by the system
-callback get_pinned_usage(Name :: atom()) -> Usage :: non_neg_integer().

%% get the number of keys in the cache
-callback get_num_entries(Name :: atom()) -> Usage :: non_neg_integer().