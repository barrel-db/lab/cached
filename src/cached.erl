%%
%% Copyright (C) 2017 Benoit Chesneau
%%

-module(cached).
-author("benoitc").
-behaviour(supervisor).

%% cache API
-export([
  new_groupcache/3,
  delete_groupcache/1,
  set_next_tier/2,
  lookup/2,
  insert/3,
  delete/2
]).


%% internal api
-export([
  start_link/3,
  local_lookup/2,
  local_insert/3,
  local_delete/2,
  local_usage/1,
  local_pinned_usage/1,
  local_num_entries/1
]).

%% supervisor callback
-export([init/1]).

-spec new_groupcache(Name :: atom(), Peers :: cached_peer:group(), Options :: cached_cache:cache_options()) -> ok.
new_groupcache(Name, Peers, Options) ->
  case supervisor:start_child(cached_cache_sup, [Name, Peers, Options]) of
    {ok, _Pid} -> ok;
    {error, {already_started, _Pid}} ->
      case ets:info(Name, name) of
        undefined -> erlang:error(badarg);
        _ -> ok
      end
  end.

delete_groupcache(Name) ->
  supervisor:terminate_child(cached_cache_sup, whereis(Name)).

set_next_tier(Name, NextTier) ->
  ets:insert(Name, {next_tier, NextTier}).

lookup(Name, Key) ->
  call(Name, lookup, [Name, Key]).

insert(Name, Key, Value) ->
  call(Name, insert, [Name, Key, Value]).

delete(Name, Key) ->
  call(Name, delete, [Name, Key]).

local_lookup(Name, Key) ->
  case local_call(Name, lookup, [Key]) of
    {ok, Val} -> {ok, Val};
    error ->
      lookup_next(next_tier(Name), Name, Key)
  end.

lookup_next(undefined, Name, Key) ->
  lookup_fallback(Name, Key);
lookup_next(Next, Name, Key) ->
  case cached_client:lookup(peers(Name), Next, Key) of
    {ok, Val} -> {ok, Val};
    error ->
      lookup_fallback(Name, Key)
  end.

lookup_fallback(Name, Key) ->
  case fill_cache(Name) of
    undefined ->
      error;
    Fun ->
      Val = Fun(Name, Key),
      true = call(Name, insert, [Name, Key, Val]),
      Val
  end.

local_insert(Name, Key, Value) ->
  local_call(Name, insert,[Key, Value]).

local_delete(Name, Key) ->
  local_call(Name, delete, [Key]).

local_usage(Name) ->
  local_call(Name, get_usage, []).

local_pinned_usage(Name) ->
  local_call(Name, get_pinned_usage, []).

local_num_entries(Name) ->
  local_call(Name, get_num_entries, []).

start_link(Name, Peers, Options) ->
  ServerName = case proplists:get_value(via, Options) of
                 undefined -> {local, Name};
                 Mod -> {via, Mod, Name}
               end,
  supervisor:start_link(ServerName, ?MODULE, [Name, Peers, Options]).

init([Name, Peers, Options]) ->
  %% join the peers
  ok = join(Peers, Name),
  
  %% set specs
  CacheName = cache_name(Name),
  CacheModule = cache_module(Options),
  
  %% setup next tier if needed.
  %% On eviction, we insert the data on the tier
  NextTier = proplists:get_value(next_tier, Options),
  Options1 = maybe_evict(NextTier, Peers, Options),
  
  %% initialize the cache
  Cache = #{
    id => cache,
    start => {CacheModule, start_link, [CacheName, Options1]},
    restart => transient,
    shutdown => 2000,
    type => worker,
    modules => [cached_lru]
  },

  %% set the config
  FillCacheFun = proplists:get_value(fill_cache, Options),

  _ = ets:new(Name, [named_table, set, public]),
  ets:insert(Name, [
    {pid, self()},
    {peers, Peers},
    {cache, {CacheModule, CacheName}},
    {fill_cache, FillCacheFun},
    {next_tier, NextTier},
    {options, Options1}
  ]),
  
  %% finally start the supervisor
  SupFlags = #{ strategy => one_for_all, intensity => 4, period => 3600},
  {ok, {SupFlags, [Cache]}}.

cache_name(Name) ->
  list_to_atom(
    "groupcache_cache_" ++ atom_to_list(Name)
  ).

cache_module(#{ cache_type := lru }) -> cached_lru;
cache_module(#{ cache_type := volatile }) -> cached_volatile_cache;
cache_module(#{ cache_type := ModCache }) when is_atom(ModCache) ->
  _ = code:ensure_loaded(ModCache),
  case erlang:function_exported(ModCache, start_link, 2) of
    false ->
      erlang:error(badarg);
    true ->
      ModCache
  end;
cache_module(_) -> cached_lru.

peers(Name) -> ets:lookup_element(Name, peers, 2).
local_cache(Name) -> ets:lookup_element(Name, cache, 2).
fill_cache(Name) -> ets:lookup_element(Name, fill_cache, 2).
next_tier(Name) -> ets:lookup_element(Name, next_tier, 2).

call(Name, Fun, Args) ->
  #{ backend := Backend } = Peers = peers(Name),
  apply(Backend, Fun, [Peers] ++ Args).

local_call(Name, Fun, Args) ->
  {CacheMod, LocalCacheName} = local_cache(Name),
  apply(CacheMod, Fun, [LocalCacheName] ++ Args).

join(Peers = #{ backend := Backend }, Name) ->
  Backend:join(Peers, Name, self()).

%% TODO: make it optionnal?
maybe_evict(undefined, _Peers, Options) -> Options;
maybe_evict(Tier, Peers, Options) ->
  EvictFun =
    fun(Key, Value) ->
      cached_client:insert(Peers, Tier, Key, Value)
    end,
  [{evict_fun, EvictFun} | Options].