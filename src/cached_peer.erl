%%
%% Copyright (C) 2017 Benoit Chesneau
%%

-module(cached_peer).
-author("benoitc").

-type group() :: #{
  backend := any(),
  any() => any()
}.

-type peer() :: any().
-type cache() :: atom().

-export_types([
  context/0,
  peer/0
]).


%% let the current process join a group
-callback join(Peers :: group(), Cache :: cache(), Pid :: pid()) -> ok | {error, any()}.

%% pick a peer in the group of peer and return it.
-callback pick_peer(Peers :: group(), Cache :: cache(), Key :: term()) -> Peer :: peer() | undefined.

%% lookup for a key among peers
-callback lookup(Peers :: group(), Cache :: cache(), Key :: term()) -> {ok, Value :: term()} | error.

%% insert a key in one of the peer picked in the group
-callback insert(Peers :: group(), Cache :: cache(), Key :: term(), Value :: term()) -> ok | {error, term()}.

%% delete a key in one of the peer picked in the group
-callback delete(Peers :: group(), Cache :: cache(), Key :: term()) -> ok.