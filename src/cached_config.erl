%%
%% Copyright (C) 2017 Benoit Chesneau
%%

-module(cached_config).
-author("benoitc").

%% API
-export([
  get/1, get/2,
  put/2,
  delete/1,
  start_link/0
]).


-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3
]).

-include_lib("syntax_tools/include/merl.hrl").

-define(CONFIG_MOD, cached_config_mod).

%% ----------------------
%% public API

-spec get(Key :: atom()) -> any() | undefined.
get(Key) -> get(Key, undefined).

-spec get(Key :: atom(), Default :: any()) -> any().
get(Key, Default) when is_atom(Key) ->
  try ?CONFIG_MOD:Key()
  catch
    error:undef ->  Default
  end;
get(_, _) ->
  erlang:error(badarg).

-spec put(Key :: atom(), Val :: any()) -> ok.
put(Key, Val) when is_atom(Key) ->
  case gen_server:call(?MODULE, {put, Key, Val}) of
    ok -> ok;
    error -> erlang:error(badarg)
  end;
put(_, _) ->
  erlang:error(badarg).

-spec delete(Key :: atom()) -> erlang:error(badarg).
delete(Key) when is_atom(Key) ->
  gen_server:call(?MODULE, {delete, Key});
delete(_) ->
  erlang:error(badarg).

start_link() ->
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

%% ----------------------
%% gen_server callbacks

init([]) ->
  _ = build_config(#{}),
  {ok, #{}}.

handle_call({put, Key, Val}, _From, Config) ->
  NewConfig = Config#{ Key => Val},
  Res = try build_config(NewConfig)
        catch
          error:_Error -> error
        end,
  {reply, Res, NewConfig};

handle_call({delete, Key}, _From, Config) ->
  NewConfig = maps:remove(Key, Config),
  ok = build_config(NewConfig),
  {reply, ok, NewConfig};

handle_call(_Msg, _From, Config) ->
  {reply, bad_call, Config}.

handle_cast(_Msg, Config) -> {noreply, Config}.

handle_info(_Info, Config) -> {noreply, Config}.

terminate(_Reason, _Config) ->
  _ = code:delete(?CONFIG_MOD),
  ok.

code_change(_Vsn, State, _Extra) -> {ok, State}.

%% ----------------------
%% build config

build_config(Config) ->
  Module = ?Q("-module(" ++ atom_to_list(?CONFIG_MOD) ++ ")."),
  {Functions, Exported} = maps:fold(
    fun(K, V, {Functions1, Exported1}) ->
      {
        [make_function(K,V) | Functions1],
        [?Q("-export([" ++ atom_to_list(K) ++ "/0]).") | Exported1]
      }
    end,
    {[], []},
    Config),
  Forms = lists:flatten([Module, Exported, Functions]),
  merl:compile_and_load(Forms, [verbose]),
  _ = code:purge(?CONFIG_MOD),
  ok.

make_function(K, V) ->
  Cs = [?Q("() -> _@V@")],
  F = erl_syntax:function(merl:term(K), Cs),
  ?Q("'@_F'() -> [].").