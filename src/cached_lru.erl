%%
%% Copyright (C) 2017 Benoit Chesneau
%%


-module(cached_lru).
-behaviour(gen_server).
-behaviour(cached_cache).

-export([
  insert/3,
  lookup/2,
  delete/2,
  clear/1,
  get_usage/1,
  get_pinned_usage/1,
  get_num_entries/1
]).

-export([
  start_link/2,
  stop/1
]).

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         code_change/3,
         terminate/2]).

-define(MAX_MEMORY, 4*1024*1024).
-define(MAX_MEMORY_SAMPLE, 5).
-define(POOL_SIZE, 16).
-define(GC_INTERVAL, 300).

%% public API

insert(Name, Key, Value) ->
  ets:insert(Name, {Key, Value, erlang:monotonic_time()}).

lookup(Name, Key) ->
  case lookup_1(Name, Key) of
    {ok, Value, _TS} -> {ok, Value};
    error -> error
  end.

lookup_1(Name, Key) ->
  case ets:lookup(Name, Key) of
    [] -> error;
    [{Key, Value, TS}] -> {ok, Value, TS}
  end.

delete(Name, Key) ->
  ets:delete(Name, Key).

clear(Name) ->
  _ = ets:delete_all_objects(Name),
  ok.

get_usage(Name) ->
  Mem0 = maps:get(memory_0, cached_config:get(Name)),
  (ets:info(Name, memory) - Mem0).

get_pinned_usage(Name) ->
  ets:info(Name, memory).

get_num_entries(Name) ->
  ets:info(Name, size).
  

%% internal API

start_link(Name, Options) ->
  gen_server:start_link({local, Name}, ?MODULE, [Name, Options], []).

stop(Name) ->
  gen_server:call(Name, delete).

init([Name, Options]) ->
  %% init shards
  ok = init_tab(Name, Options),
  %% get options
  Interval = proplists:get_value(gc_interval, Options, ?GC_INTERVAL),
  MaxMemory = proplists:get_value(max_memory, Options, ?MAX_MEMORY),
  MaxMemorySample = proplists:get_value(max_memory_sample, Options, ?MAX_MEMORY_SAMPLE),
  EvictFun = proplists:get_value(evict_fun, Options),
  
  %% get initial memory usage to remove it from the max memory used
  Sz = ets:info(Name, memory),
  
  %% store metadata
  Meta = #{ tab => Name, memory_0 => Sz},
  ok = cached_config:put(Name, Meta),
  
  %% set first interval
  _ = erlang:send_after(Interval, self(), timeout),
  State = #{tab => Name,
            max_memory => MaxMemory,
            max_memory_sample => MaxMemorySample,
            gc_interval => Interval,
            memory_0 => Sz,
            evict_fun => EvictFun,
            key_pool => [] },
  {ok, State}.


terminate(_Reason, #{ tab := Tab }) ->
  %% delete the tab explicitely
  _ = ets:delete(Tab),
  ok.

code_change(_Vsn, State, _Extra) ->
  {ok, State}.

handle_call(_Msg, _From, State) ->
  {reply, bad_call, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.

handle_info(timeout, State) ->
  #{ max_memory := Max,
     gc_interval := Interval } = State,
  Mem = memory_used(State),
  NewState = case Mem > Max of
               true ->
                 resize(update_pool(State));
               false ->
                 update_pool(State)
             end,
  _ = erlang:send_after(Interval, self(), timeout),
  {noreply, NewState};


handle_info(_Info, State) ->
  {noreply, State}.

%% ----------------------
%% cache management

memory_used(#{ tab := Tab, memory_0 := Mem0 }) ->
  ets:info(Tab, memory) - Mem0.

resize(State = #{ key_pool := Pool }) ->
  resize(Pool, memory_used(State), State).

resize([{Key, _} | Rest], Used, State = #{ max_memory := Max }) when Used > Max  ->
  ok = evict(Key, State),
  resize(Rest, memory_used(State), State#{ key_pool => Rest });
resize([], Used, State = #{ max_memory := Max }) when Used > Max  ->
  resize(update_pool(State));
resize(_, _, State) ->
  State.

evict(Key, #{tab := Tab, evict_fun := undefined}) ->
  _ = ets:delete(Tab, Key),
  ok;
evict(Key, #{tab := Tab, evict_fun := EvictFun}) ->
  case ets:take(Tab, Key) of
    [{Key, Value, _Ts}] ->
      _ = spawn(fun() -> EvictFun(Key, Value) end),
      ok;
    [] ->
      ok
  end.

update_pool(State = #{ key_pool := Pool }) ->
  Sample = get_sample(State),
  State#{ key_pool => update_pool(Sample, Pool) }.

update_pool([], Pool) -> Pool;
update_pool(Sample, Pool) ->
  lists:sublist(
    lists:usort(
      fun({_, T1}, {_, T2}) when T1 =< T2 -> true;
         (_, _) -> false
      end,
      Pool ++ Sample
     ),
    1,
    ?POOL_SIZE
   ).

get_sample(#{ tab := Tab, max_memory_sample := Max }) ->
  get_sample([], Tab, Max).

get_sample(Acc, Tab, Max) when length(Acc) < Max ->
  case ets:info(Tab, size) of
    Sz when Sz >= Max, Sz > 0 ->
      [{K, _V, T} | _] = ets:slot(Tab, uniform(Sz)),
      get_sample([{K, T} | Acc], Tab, Max);
    _ ->
      []
  end;
get_sample(Acc, _Tab, _Max) ->
  Acc.

uniform(Sz) ->
  rand:uniform(Sz) - 1.

init_tab(Name, Options) ->
  Name = ets:new(Name, ets_options(Options)),
  ok.

ets_options(Options) ->
  ReadConcurrency = proplists:get_value(read_concurrency, Options, true),
  WriteConcurrency = proplists:get_value(write_concurrency, Options, true),
  [
    ordered_set, named_table, public,
    {read_concurrency, ReadConcurrency},
    {write_concurrency, WriteConcurrency}
  ].