%%
%% Copyright (C) 2017 Benoit Chesneau
%%
-module(cached_tier_file).
-author("benoitc").
-behaviour(gen_server).

%% API
-export([
  open/1,
  open_read/1,
  close/1,
  delete/1,
  append/3,
  read/3, read/2,
  iterator/1,
  next/1
]).

%% gen_server callbacks
-export([
  init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  code_change/3,
  terminate/2
]).

-define(RETRY_TIME_MS, 1000).
-define(MAX_RETRY_TIME_MS, 10000).
-define(SIZE_BLOCK, 16#1000).
-define(DEFAULT_READ_BUFFER, ?SIZE_BLOCK * 8). %% 32 MB
-define(MAX_SIZE, 1024 * 1024). %% 1MB
-define(MAGIC, 1).

-record(iterator, {blockfile, offset}).

open(FilePath) ->
  ReadBufferSize = application:get_env(cached, read_buffer, ?DEFAULT_READ_BUFFER),
  Options = [raw, read, append, binary, {read_ahead, ReadBufferSize}],
  proc_lib:start_link(?MODULE, init, [{FilePath, Options}]).

open_read(FilePath) ->
  ReadBufferSize = application:get_env(cached, read_buffer, ?DEFAULT_READ_BUFFER),
  Options = [raw, read, binary, {read_ahead, ReadBufferSize}],
  proc_lib:start_link(?MODULE, init, [{FilePath, Options}]).

close(BlockFile) ->
  gen_server:call(BlockFile, close).

delete(FilePath) ->
  case file:delete(FilePath) of
    ok -> ok;
    {error, enoent} -> ok;
    Error -> Error
  end.

append(BlockFile, Key, Value) ->
  KeyBin = term_to_binary(Key),
  ValueBin = term_to_binary(Value),
  KVBin = << KeyBin/binary, ValueBin/binary >>,
  CRC = erlang:crc32(KVBin),
  Bin = << ?MAGIC, CRC:32,
           (byte_size(KeyBin)):32,
           (byte_size(ValueBin)):32,
           KVBin/binary >>,
  gen_server:call(BlockFile, {append, Bin}).
  
read(BlockFile, Pos, Size) ->
  case gen_server:call(BlockFile, {read, Pos, Size}) of
    {ok, << ?MAGIC, Crc:32, KSz:32, VSz:32, KVBin/binary >>} ->
      case validate_checksum(Crc, KVBin) of
        true ->
          << KeyBin:KSz/binary, ValueBin:VSz/binary >> = KVBin,
          {ok, binary_to_term(KeyBin), binary_to_term(ValueBin)};
        false ->
          {error, corrupted_data}
      end;
    Error ->
      Error
  end.

iterator(BlockFile) -> #iterator{blockfile=BlockFile, offset=0}.

next(#iterator{blockfile=BlockFile, offset=Offset} = Iterator) ->
  case read(BlockFile, Offset) of
    {ok, Key, Value, NewOffset} ->
      NewIterator = Iterator#iterator{offset=NewOffset},
      {ok, {Key, Value}, NewIterator};
    Error ->
      Error
  end.

read(BlockFile, Pos) ->
  case gen_server:call(BlockFile, {read, Pos}) of
    {ok, {Crc, KSz, VSz, KVBin}, NewPos} ->
      case validate_checksum(Crc, KVBin) of
        true ->
          << KeyBin:KSz/binary, ValueBin:VSz/binary >> = KVBin,
          {ok, binary_to_term(KeyBin), binary_to_term(ValueBin), NewPos};
        false ->
          {error, corrupted_data}
      end;
    Error ->
      Error
  end.

%% ------------------
%% gen_server

init({Path, Options}) ->
  case try_open_fd(Path, Options, ?RETRY_TIME_MS, ?MAX_RETRY_TIME_MS) of
    {ok, Fd} ->
      ReadOnly = (lists:member(append, Options) =:= false),
      {ok, Eof} = file:position(Fd, eof),
      proc_lib:init_ack({ok, self()}),
      InitState = #{ path => path, fd => Fd, eof => Eof, read_only => ReadOnly},
      gen_server:enter_loop(?MODULE, [], InitState);
    Error ->
      proc_lib:init_ack(Error)
  end.

handle_call({append, Bin}, _From, State = #{ fd := Fd, eof := Eof }) ->
  case append_blocks(Bin, Fd, Eof) of
    {ok, Pos, Sz} ->
      _ = file:datasync(Fd),
      {reply, {ok, Eof, Sz}, State#{ eof => Pos }};
    Error ->
      {reply, Error, State}
  end;

handle_call({read, Pos, Size}, _From, State = #{ fd := Fd }) ->
  Reply = file:pread(Fd, Pos, Size),
  {reply, Reply, State};

handle_call({read, Pos}, _From, State = #{ fd := Fd }) ->
  Reply = pread(Fd, Pos),
  {reply, Reply, State};

handle_call(close, _From, State) ->
  {stop, normal, ok, State};

handle_call(_Msg, _From, State) ->
  {reply, badcall, State}.

handle_cast(_Msg, State) -> {noreply, State}.

handle_info(_Info, State) -> {noreply, State}.

terminate(_Reason, #{ fd := nil }) ->
  ok;
terminate(_Reason, #{ fd := Fd, read_only := true }) ->
  file:close(Fd);
terminate(_Reason, #{ fd := Fd }) ->
  _ = file:sync(Fd),
  _ = file:close(Fd),
  ok.

code_change(_Vsn, State, _Extra) ->
  {ok, State}.

%% ------------------
%% helpers

try_open_fd(FilePath, Options, _Timewait, TotalTimeRemain)
  when TotalTimeRemain < 0 ->
  % Out of retry time.
  % Try one last time and whatever we get is the returned result.
  file:open(FilePath, Options);
try_open_fd(FilePath, Options, Timewait, TotalTimeRemain) ->
  case file:open(FilePath, Options) of
    {ok, Fd} ->
      {ok, Fd};
    {error, emfile} ->
      error_logger:info_msg(
        "~s: Too many file descriptors open, waiting ~pms to retry",
        [?MODULE_STRING, Timewait]
      ),
      receive
      after Timewait ->
        try_open_fd(FilePath, Options, Timewait,
                    TotalTimeRemain - Timewait)
      end;
    {error, eacces} ->
      error_logger:info_msg(
        "~s: eacces error opening file ~p waiting ~pms to retry",
        [?MODULE_STRING, FilePath, Timewait]
      ),
      receive
      after Timewait ->
        try_open_fd(FilePath, Options, Timewait,
                    TotalTimeRemain - Timewait)
      end;
    Error ->
      Error
  end.

validate_checksum(Crc, Bin) ->
  (Crc =:= erlang:crc32(Bin)).

append_blocks(Bin, Fd, Pos) ->
  Start = Pos rem ?SIZE_BLOCK,
  if
    Start == 0 ->
      append_blocks(Bin, Fd, Pos, []);
    true ->
      Len = erlang:min(?SIZE_BLOCK - Start, byte_size(Bin)),
      << Part:Len/binary, Rest/binary >> = Bin,
      append_blocks(Rest, Fd, Pos + Len, [Part])
  end.

append_blocks(<<>>, Fd, Pos, Blocks) ->
  case file:write(Fd, lists:reverse(Blocks)) of
    ok ->   {ok, Pos, iolist_size(Blocks)};
    Error -> Error
  end;
append_blocks(<< Block:?SIZE_BLOCK/binary, Rest/binary >>, Fd, Pos, Blocks) ->
  append_blocks(Rest, Fd, Pos + ?SIZE_BLOCK, [Block | Blocks]);
append_blocks(Block, Fd, Pos, Blocks)  ->
  append_blocks(<<>>, Fd, Pos + byte_size(Block), [Block | Blocks]).

pread(Fd, Pos) ->
  case file:pread(Fd, Pos, 13) of
    {ok, << ?MAGIC, Crc:32, KSz:32, VSz:32 >>} ->
      case file:pread(Fd, Pos+13, KSz + VSz) of
        {ok, Data} ->
          NewPos = Pos + 13 + KSz + VSz,
          {ok, {Crc, KSz, VSz, Data}, NewPos};
        Error ->
          Error
      end;
    Error ->
      Error
  end.